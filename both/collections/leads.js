Leads = new Mongo.Collection('leads');
Leads.helpers({});
Leads.before.insert(function (userId, doc) {
    doc.createdAt = moment().toDate();
});