ImportController = AppController.extend({
  waitOn: function() {
    return this.subscribe('items');
  },
  data: {
    items: Items.find({})
  },
  onAfterAction: function () {
    Meta.setTitle('CSV Importer');
  }
});

ImportController.events({
  'click [data-action=doSomething]': function (event, template) {
    event.preventDefault();
  }
});
