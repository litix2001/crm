Router.route('/', {
  name: 'home',
  waitOn: function(){
    Meteor.subscribe('leads');
    return [];
  }
});

Router.route('/import', {
  name: 'import',
  waitOn: function(){
    Meteor.subscribe('leads');
    return [];
  },
  controller: 'ImportController'
});

Router.route('/users',{
  name:'users',
  waitOn: function(){
    Meteor.subscribe('users');
    return [];
  },
});

Router.route('/search', {
  name: 'search',
  waitOn: function(){
    Meteor.subscribe('leads');
    return [];
  },
});

Router.plugin('ensureSignedIn', {
 only: ['import', 'home', 'users']
 });
