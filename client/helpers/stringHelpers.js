Template.registerHelper('truncate', function(string, length) {
  var cleanString = s(string).stripTags();
  return s(cleanString).truncate(length);
});
Template.registerHelper('render', function(array, val){
  var newD = "";
  for(var i = array.length-1; i--;){
    if (array[i] == val){
      newD = newD + '<option value="' + array[i] +'" selected="selected">'+array[i]+'</option>';
    }else{
      newD = newD + '<option value="' + array[i] +'">'+array[i]+'</option>';
    }
  }
  return Spacebars.SafeString(newD);
});
Template.registerHelper("isEqual", function (g, r) {
  var result = "";
  if(g === r){
      result = 'selected';
  }
  return  Spacebars.SafeString(result);
});
Template.registerHelper('listString', function(array, val){
  var newD = "";
  for(var i = array.length-1; i--;){
    var r  = array[i].toLowerCase();
    if (r == val){
      newD = newD + '<option value="' + r +'" selected="selected">'+array[i]+'</option>';
    }else{
      newD = newD + '<option value="' + r +'">'+array[i]+'</option>';
    }
  }
  return Spacebars.SafeString(newD);
});
Template.registerHelper('tel', function(str){
  var newString = str.replace(/[^A-Z0-9]+/ig, "");
  return newString;
});
Template.registerHelper('formatDate', function(date) {
  date = new Date(date);
  return date.toLocaleDateString();
});

Template.registerHelper('formatPhoneNumber', function(s) {
  var s2 = (""+s).replace(/\D/g, '');
  var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
    return (!m) ? ""  : "(" + m[1] + ") " + m[2] + "-" + m[3];
});