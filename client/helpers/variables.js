Template.registerHelper('statesList', function(){
    var states = ["---","AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY","AU","AB","BC","SK"];
    return states.reverse();
});

Template.registerHelper('referralTypeList', function(){
    return ["Mass Emailer","Data Entry", "Craig List", "Client Referral", "Cold Call", "Shane Referral", "Shane Retail", "Yellow Book", "Pat Leads", "Google","---"];
});

Template.registerHelper('actionTypeList', function(){
    return ["Client Follow Up", "Needs Call", "Needs Email", "Needs Contract", "Needs Research","No Action Needed" ,"----"];
});

Template.registerHelper('statusList', function(){
    return ["Bad Data","Ex Clients","Current","Do Not Call","Hot Lead","In Progress","Sent Email","Left Message","Called","Not Called","--"];
});

Template.registerHelper('companyTypeList', function(){
    return ["Retail","Web Development","--"];
});

Template.registerHelper('timeZoneList', function(){
    return ["Alaska","Hawaii","Australia","CST","MST","AST","PST","EST","---"];
});

Template.registerHelper('RoleList', function(){
    return ['Administrator', 'Agent', '--'];
});

Template.registerHelper('salesAgentList', function(){
    return ["Litix","Khyle","Shane","No Agent","---"];
});

Template.registerHelper('prioritytList', function(){
    return ["Low","Medium","High","Not Set","---"];
});

Template.registerHelper('perPageList', function(){
    return ["250", "200","150","100","50", "---"];
});

Template.registerHelper('timeZoneStates', function(){
    var list = {
        WA:{
            name: 'Washington',
            timezone: ['PST','PDST']
        },
        OR:{
            name: 'Oregon',
            timezone: ['PST','PDST']
        },
        NV:{
            name: 'Nevada',
            timezone: ['PST','PDST']
        },
        ID:{
            name: 'Idaho',
            timezone: ['PST','PDST']
        },
        CA:{
            name: 'California',
            timezone: ['PST','PDST']
        }
    }

    console.log(list);
    return list;
});