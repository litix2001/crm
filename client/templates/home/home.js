Session.setDefault('pager', 0);
Session.setDefault('filter', "");
Session.setDefault('filterData', 0);
Session.setDefault('totalRecords', 0);
Session.setDefault('filterName', 'default');
Session.setDefault('filterText', '');
Session.setDefault('numberOfPages', 0);
Session.setDefault('postPerPage', 50);
Session.setDefault('upcomingDate', 0);
Session.setDefault('tz', 'EST');

Meteor.call('countRecords', function(error, result){
    Session.set('totalRecords', result);
});
var options = {
    //keepHistory: 1000 * 60 * 5,
    localSearch: false,
    limit: Session.get('postPerPage'),
    page: Session.get('pager')
};
var fields = ['companyemail','companyname', 'status'];

LeadSearch = new SearchSource('leads', fields, options);

function runFilter(){
    var getText = $('#search-box').val();
    var filterName = Session.get('filterName');
    var options = "";
    var min = $('.startDate').val();
    var max = $('.endDate').val();

    if(filterName == 'dropdown'){
        options = {filters:getFilters(), filterName: filterName, skip: Session.get('pager'), limit: Session.get('postPerPage')};
        return LeadSearch.search(getText, options);
    }else if(filterName == 'textbox'){
        options = {filterName: filterName, skip: Session.get('pager'), limit: Session.get('postPerPage')};
        return LeadSearch.search(Session.get('filterText'), options);
    }else if(filterName == 'date'){
        options = {min: min, max: max, filterName: "date", skip: Session.get('pager'), limit:Session.get('postPerPage')};
        return LeadSearch.search("n/a", options);
    }else{
        options = {filterName: filterName, skip: Session.get('pager'), limit:Session.get('postPerPage')};
        return LeadSearch.search(getText, options);
    }
}
function getFilters(){
    var fi = $('.filter-data');
    var myObj = {};
    var options = "";
    for (var i = 0; i < fi.length; i += 1) {
        if(fi[i].value != "0"){
            myObj[fi[i].name] = fi[i].value;
        }
    }
    return myObj;
}

Template.searchResult.rendered = function() {
    LeadSearch.search("");
    $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY',
        dayViewHeaderFormat: 'MMMM YYYY',
        extraFormats: false,
        stepping: 1,
        minDate: false,
        maxDate: false,
        useCurrent: false,
        collapse: true,
        locale: moment.locale(),
        defaultDate: false,
        disabledDates: false,
        enabledDates: false,
        icons: {
            time: 'glyphicon glyphicon-time',
            date: 'glyphicon glyphicon-calendar',
            up: 'glyphicon glyphicon-chevron-up',
            down: 'glyphicon glyphicon-chevron-down',
            previous: 'glyphicon glyphicon-chevron-left',
            next: 'glyphicon glyphicon-chevron-right',
            today: 'glyphicon glyphicon-screenshot',
            clear: 'glyphicon glyphicon-trash',
            close: 'glyphicon glyphicon-remove'
        },
        useStrict: false,
        sideBySide: false,
        daysOfWeekDisabled: [],
        calendarWeeks: true,
        viewMode: 'days',
        toolbarPlacement: 'default',
        showTodayButton: true,
        showClear: true,
        showClose: true,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'auto'
        },
        widgetParent: null,
        ignoreReadonly: false,
        keepOpen: false,
        focusOnShow: true,
        inline: false,
        keepInvalid: false,
        datepickerInput: '.datepickerinput',
    });
};
Template.searchResult.helpers({
    'query': function(){
        //searchText = LeadSearch.getCurrentQuery();
        //return searchText;
    },
    'numberOfPages': function(){
        var pager = [];
        var itemsPerPage = Session.get('postPerPage');
        var totalPages = Session.get('numberOfPages');
        var count = 0;

        for (var i = 1; i <= totalPages; i += 1) {
                var obj = {page: i, data: Number(count)};
                count = count + itemsPerPage;
            pager.push(obj);
        }
        return pager;
    },
    countRecords: function(){
        var d = LeadSearch.getMetadata();
        //postPerPage = 50
        var x = d.count / Session.get('postPerPage');
        var totalpages =  Math.ceil(x);
        Session.set('numberOfPages', totalpages);

        //Debug Console
        //console.log("Total Pages : " + totalpages);
        //console.log("Records Found :"+ d.count);
        return d.count;
    },
    getPackages: function() {
        return LeadSearch.getData({});
    },
    isLoading: function() {
        return LeadSearch.getStatus().loading;
    },
    previewsText: function(){
        if(Number(Session.get('pager')) < Session.get('postPerPage')){
            return;
        }else{
            return (Number(Session.get('pager')- Session.get('postPerPage')) + " - " + Number(Session.get('pager')));
        }
    },
    nextText : function(){
        return (Number(Session.get('pager') + Session.get('postPerPage')) + " - " + Number(Session.get('pager') + 40));
    },
    showingText : function(){
        return (Number(Session.get('pager')) + " - " + Number(Session.get('pager') + Session.get('postPerPage'))  );
    },
    filterData : function(){
        var fd = Session.get('filterData');
        if(fd == 0){
            return  '(Default Filter)';
        }else if(fd == 1){
            return  '(Search Box Filter)';
        }else if (fd == 2){
            return  '(Dropdown Filter)';
        }else if (fd == 3){
            return  '(Date Filter)';
        }
    },
    selectDeliveryStatus: function(optionText){
        if(optionText === this.deliveryStatus){
            return 'selected'
        }
    }
});
Template.createlLeadForm.helpers({
    tz: function(){
        var tz = Session.get('tz');
        return tz;
    },
    selectDeliveryStatus: function(optionText){
        var tz = Session.get('tz');
        if(optionText === tz){
            return 'selected'
        }
    }
});
Template.createlLeadForm.events({
    'change #create-companystate': function(e,t){
        var sel = e.target;
        var categId = sel.options[sel.selectedIndex].getAttribute('data-id');
        Session.set('tz', "");
        Session.set('tz', categId);
    },
});

Template.home.events({
    'change .perPage': function(evt, tpl){
        var limit = $('.perPage').val();
        Session.set('postPerPage',Number(limit));
        //var options = {filterName: "default", limit: Session.get('postPerPage')};
        //LeadSearch.search("_", options);
        runFilter();
    },
    'click .pager': function(evt, tpl){
        Session.set('pager',Number(evt.target.dataset.skip));
        runFilter();
    },
    'click .viewAllBtn': function(){
        Session.set('filterName', 'default');
        Session.set('filterData', 0);
        Session.set('pager', 0);
        runFilter();
    },
    'keyup #search-box': _.throttle(function(e) {
        var text = $(e.target).val().trim();
        Session.set('filterData', 1);
        Session.set('filterName', 'textbox');
        Session.set('filterText', text);
        Session.set('pager', 0);
        /*options = {filterName: "textbox", skip: Session.get('pager')};
        LeadSearch.search(text, options);*/
        runFilter();
    }, 200),
    'change .filter-data': function(evt, t){
        Session.set('filterData', 2);
        Session.set('pager', 0);
        Session.set('filterName', 'dropdown');
        var myObj = getFilters();
        var options = {filters:myObj, filterName: "dropdown", skip: Session.get('pager'), limit: Session.get('postPerPage')};
        LeadSearch.search("na", options);
    },
    'click  .filterByDate': function(evt, tpl){
        Session.set('filterName', 'date');
        Session.set('filterData', 3);
        var min = $('.startDate').val();
        var max   = $('.endDate').val();
        var x = $('.nextDate').is(':checked');

        if(x  == false){
            options = {min: min, max: max, filterName: "date" ,limit: Session.get('postPerPage')};
            return LeadSearch.search("n/a", options);
        }else if(x == true){
            options = {min: min, max: max, filterName: "upComingDate", limit: Session.get('postPerPage')};
            return LeadSearch.search("n/a", options);
        }
    },
    'click  .next': function(evt, tpl) {
        Session.set('pager', Number(Session.get('pager')) + 20);
        runFilter();
    },
    'click  .previews': function(evt, tpl){
        if(Number(Session.get('pager')) > 19){
            Session.set('pager', Number(Session.get('pager')) - 20);
            runFilter();
        }
    },
    'click  .leadDelete': function(evt, tpl){
        var id = evt.target.dataset.leadDelete;
        var options = {filterName: "delete",skip: Session.get('pager'), limit: Session.get('postPerPage'), id:id};
        Session.set('filterName', 'default');
        Session.set('filterData', 0);
        Session.set('pager', 0);

        if(id != undefined){
            if (confirm('Are you sure you want to delete this thing into the database? id = ' + id)) {LeadSearch.search("na", options);}
        }
    },
    'click  .updateLeadbtn': function(e, t) {
        var id = e.target.dataset.targetId;
        var fi = $('.'+id);
        var myObj = {};
        for (var i = 0; i < fi.length; i += 1) {
            myObj[fi[i].name] = fi[i].value;
        }
        var protomatch = /^(https?|ftp):\/\//;
        Leads.update({_id: myObj.leadId},{
            $set: {
                "status" : myObj.status,
                "referaltype" : myObj.referaltype,
                "actiontype" : myObj.actiontype,
                "companytype" : myObj.companytype,
                "priority" : myObj.priority,
                "timezone" : myObj.timezone,
                "companyname" : myObj.companyname.replace(/,/gi,' '),
                "companyphone" : myObj.companyphone.replace('+1 ',''),
                "companyemail" : myObj.companyemail,
                "companyskype" : myObj.companyskype,
                "companyaddress" : myObj.companyaddress.replace(/,/gi,' '),
                "companyaddress1" : myObj.companyaddress1.replace(/,/gi,' '),
                "companycity" : myObj.companycity,
                "companystate" : myObj.companystate,
                "comapnyzip" : myObj.comapnyzip,
                "companywebsite" : myObj.companywebsite.replace(protomatch, ""),
                "comments" : myObj.comments.replace(/,/gi,' '),
                "contactname" : myObj.contactname.replace(/,/gi,' '),
                "contactphone" : myObj.contactphone.replace('+1 ',''),
                "contactmobilephone" : myObj.contactmobilephone.replace('+1 ',''),
                "initialwriteup" : myObj.initialwriteup.replace(/,/gi,' '),
                'nextdate': "",
                "lastactivatedate": new Date(),
            }
        });
        Leads.update({_id: myObj.leadId},{
            $set: {
                'nextdate': new Date(myObj.nextdate),
            }
        });
        setTimeout(function(myobj){
            Session.set('filterData', 1);
            Session.set('filterName', 'textbox');
            Session.set('filterText', "textbox");
            Session.set('pager', 0);
            options = {filterName: "textbox", skip: Session.get('pager')};
            LeadSearch.search(myObj.companyname, options);
            $('.closeEdit').click();
        },500);
    },
    'click  .createLead': function(){

        var fi = $('.createFields');
        var myObj = {};
        for (var i = 0; i < fi.length; i += 1) {
            if(fi[i].value == ""){
                myObj[fi[i].name] = "N/A";
            }else{
                myObj[fi[i].name] = fi[i].value;
            }

        }
        //Validate
        console.log(myObj);
        var protomatch = /^(https?|ftp):\/\//;
        var result = Leads.insert({
            "status" : myObj.status,
            "referaltype" : myObj.referaltype,
            "actiontype" : myObj.actiontype,
            "agentid": "N/A",
            "creatorid": Meteor.userId(),
            "companytype" : myObj.companytype,
            "priority" : myObj.priority,
            "timezone" : myObj.timezone,
            "salesagent": myObj.salesagent,
            "companyname" : myObj.companyname.replace(/,/gi,' '),
            "companyphone" : myObj.companyphone.replace('+1 ',''),
            "companyemail" : myObj.companyemail,
            "companyskype" : myObj.companyskype,
            "companyaddress" : myObj.companyaddress.replace(/,/gi,' '),
            "companyaddress1" : myObj.companyaddress1.replace(/,/gi,' '),
            "companycity" : myObj.companycity.replace(/,/gi,' '),
            "companystate" : myObj.companystate,
            "comapnyzip" : myObj.comapnyzip,
            "companywebsite" : myObj.companywebsite.replace(protomatch, ""),
            "lastactivatedate":  new Date(),
            'nextdate': new Date(myObj.nextdate),
            "comments" : myObj.comments.replace(/,/gi,' '),
            "contactname" : myObj.contactname.replace(/,/gi,' '),
            "contactphone" : myObj.contactphone.replace('+1 ',''),
            "contactmobilephone" :myObj.contactmobilephone.replace('+1 ',''),
            "initialwriteup" : myObj.initialwriteup.replace(/,/gi,' '),
            "actionlog": "N/A",
            "ipaddress": "N/A",
            "needaction":0,
            "createdAt": new Date(),
        });

        if(result != ""){
            setTimeout(function(){
                for (var i = 0; i < fi.length; i += 1) {
                    fi.val("");
                }
                Session.set('filterData', 1);
                Session.set('filterName', 'textbox');
                Session.set('filterText', "textbox");
                Session.set('pager', 0);
               // LeadSearch.search("na", options);
                $('.closeCreate').click();
                document.location.reload(true);
            },500);
        }
    },
    'click  .linktocall': function(evt,tpl){
        var id = evt.target.dataset.tableId;
        setTimeout(function(){
            $('#edit'+id).click();
        },100);
    },
    'click  .leadEdit': function(evt, tpl){
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY',
            dayViewHeaderFormat: 'MMMM YYYY',
            extraFormats: false,
            stepping: 1,
            minDate: false,
            maxDate: false,
            useCurrent: false,
            collapse: true,
            locale: moment.locale(),
            defaultDate: false,
            disabledDates: false,
            enabledDates: false,
            icons: {
                time: 'glyphicon glyphicon-time',
                date: 'glyphicon glyphicon-calendar',
                up: 'glyphicon glyphicon-chevron-up',
                down: 'glyphicon glyphicon-chevron-down',
                previous: 'glyphicon glyphicon-chevron-left',
                next: 'glyphicon glyphicon-chevron-right',
                today: 'glyphicon glyphicon-screenshot',
                clear: 'glyphicon glyphicon-trash',
                close: 'glyphicon glyphicon-remove'
            },
            useStrict: false,
            sideBySide: false,
            daysOfWeekDisabled: [],
            calendarWeeks: true,
            viewMode: 'days',
            toolbarPlacement: 'default',
            showTodayButton: true,
            showClear: true,
            showClose: true,
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'auto'
            },
            widgetParent: null,
            ignoreReadonly: false,
            keepOpen: false,
            focusOnShow: true,
            inline: false,
            keepInvalid: false,
            datepickerInput: '.datepickerinput',
        });
    }
});