// Listen to incoming HTTP requests, can only be used on the server
Session.set('message','');
Template.import.rendered = function(){
    WebApp.connectHandlers.use(function(req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        return next();
    });
};

Template.import.helpers({
    'countRecords': function(){
        Meteor.call('countRecords', function(err,result){
            return Session.set('countRecords', result);
        });
    },
    'message': function(){
        return Session.get('message');
    }
});

Template.import.events({
    'change .myFileInput' : function(evt, tpl){
        console.log('uploading event method');
        FS.Utility.eachFile(event, function(file){
            var theFile = new FS.File(file);
            Uploads.insert(theFile, function(err, fileObj){
                if(!err){
                    Meteor.call('uploadFile', fileObj._id, file.name, function(error, result){
                        console.log(error);
                        console.log(result);
                        Session.set('message','Imported ' + file.name + " "+ result + ", Records");
                    });
                }else{
                    console.log("upload error:" + err);
                    Session.set('message','An Error Occured: ' + err);
                }
            });
        })
    },
    'click .delete' : function(){

        Meteor.call('deleteAll');

        setTimeout(function(){
            $('.csvImportModalCloseBtn').click();
            Session.set('message','Deleted all data');
        },500);
    },
    'click #buttonDownload': function(event) {
        var nameFile = 'leads.csv';
        Meteor.call('download', function(err, fileContent) {
            if(fileContent){
                var blob = new Blob([fileContent], {type: "text/plain;charset=utf-8"});
                saveAs(blob, nameFile);
            }
        });
        Session.set('message','Exported File');
    }
});