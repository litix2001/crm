Meteor.subscribe('allUsers');
Template.users.rendered = function(){}
Template.editModal.helpers({
    'checkrole': function(role){
        if(role == 'administrator'){
            return 'selected';
        }else if (role == 'agent'){
            return 'selected';
        }else{
            return '';
        }
    },
});
Template.users.helpers({
    'listusers': function(){
        var users = Meteor.users.find({}).fetch();
        return users;
    },
    'checkrole': function(role){
        var text = "";
        if(role == 'administrator'){
            return true;
        }else if (role == 'agent'){
            return true;
        }else{
            return "";
        }
    },
    'userEmail': function() {  return this.emails[0].address; },
    'userRole': function() {
        return this.roles[0];
    }
});
Template.users.events({
    'submit .createForm': function(e, tpl){

        e.preventDefault();

        var fullname = tpl.find('#fullname').value;
        var emailAddress = tpl.find('#emailAddress').value;
        var userPassword = tpl.find('#password').value;
        var passwordVerify = tpl.find('#passwordVerify').value;
        var userRole = tpl.find('#userRole').value;

        var data = {
            'email':emailAddress,
            'password': userPassword,
            'name': fullname,
            role: userRole
        }

        if(userPassword === passwordVerify){
            Meteor.call('createAccount',data, function(error, result){
                setTimeout(function(){
                    $('.btnCloseUserCreate').click();
                    //console.log(result);
                    return result;
                },500);
            });
        }else{
            alert('Password dont match');
        }


    },
    'click .editForm': function(e,d){
        var id = e.target.dataset.editForm;
        var fullname = d.find('#fullname-'+id).value;
        var emailAddress = d.find('#emailaddress-'+id).value;
        var userRole = d.find('#userrole-'+id).value;
        var password = d.find('#password-'+id).value;

        var data = {
            userid : id,
            name: fullname,
            username: fullname,
            emailaddress: emailAddress,
            role: userRole,
            password: password
        }

        console.log(data);

        Meteor.call('editUser', data, function(error, result){
            setTimeout(function(){
                $('.editClose').click();
            },500);
        });
    },
    'click .userDelete': function(evt, tpl){
        var id = evt.target.dataset.userDelete;
        if(id != undefined){
            if (confirm('Are you sure you want to delete this thing into the database? id = ' + id)) {
                Meteor.users.remove({_id:id});
            }
        }
    }
});