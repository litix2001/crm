SearchSource.defineSource('leads', function(searchText, options) {
    var data = "";
    var filterName = "default";
    var skip = 0;
    var dropdown = 0;
    var filters = "";
    var limit = 50;

    if(options != null){

        if(options.filterName){
            filterName = options.filterName;
        }

        if(options.limit){
            limit = options.limit;
        }

        if(options.filters){
            filters =  options.filters;
        }
        if(options.skip){
            skip = options.skip;
        }
    }

    if(filterName == 'textbox'){
       data = filterBy(searchText, skip, limit);
    }

    if(filterName == 'dropdown'){
        data =  filterBymultiple(filters, skip, limit);
    }

    if(filterName == 'date'){
        var min = options.min;
        var max = options.max;
        data = filterByDate(min,max,skip, limit);
    }

    if(filterName == 'upComingDate'){
        var min = options.min;
        var max = options.max;
        data = filterByupComingDate(min,max,skip, limit);
    }

    if(filterName == 'default'){
        data = defaultFilter(skip, limit);
    }

    if(filterName == 'delete') {
        var id = options.id;
        Leads.remove(id);
        return {
            data: Leads.find({}, { limit: limit, sort:{ lastactivatedate: -1 }}).fetch(),
            metadata: {count: Leads.find({}).count()}
        }
    }

    return {
        data: data.records,
        metadata: {count: data.count}
    }
});
function filterBy(searchText, skip, limit){
    var results = {
        count: "",
        records: ""
    }
    var regExp = searchText;
    var selector = {$or: [
        {companyname: regExp},
        {companyemail: regExp},
        {companywebsite: regExp},
        {companyskype: regExp},
        {companyphone: regExp},
        {contactname: regExp},
        {contactphone: regExp},
        {contactmobilephone: regExp},
        {referaltype: regExp},
        {actiontype: regExp},
        {companytype: regExp},
        {timezone: regExp},
        {companyaddress: regExp},
        {companyaddress1: regExp},
        {companycity: regExp},
        {companystate: regExp},
        {comapnyzip: regExp},
        {comments: regExp},
        {status: regExp},
        {createdAt: regExp},
        {lastactivatedate: regExp},
        {initialwriteup: regExp},
    ]};
    if(skip != 0){
        results.records = Leads.find(selector, { limit: limit, skip:skip, sort:{ lastactivatedate: -1 }}).fetch();
    }else{
        results.records =  Leads.find(selector, { limit: limit, sort:{ lastactivatedate: -1 }}).fetch();
    }
    results.count  = Leads.find(selector,{}).count();
    return results;
}
function filterBymultiple(filters, skip, limit){
    var results = {
        count: "",
        records: ""
    }
    if(skip != 0) {
        results.records = Leads.find(filters, {limit: limit, skip:skip, sort:{ lastactivatedate: -1 }}).fetch();
    }else{
        results.records = Leads.find(filters, {limit: limit, sort:{ lastactivatedate: -1 }}).fetch();
    }
    results.count = Leads.find(filters,{}).count();
    return results;
}
function filterByDate(min,max,skip, limit){
    var results = {
        count: "",
        records: ""
    }
    var mi = new Date(min);
    var ma = new Date(max);

    if(mi != ma){
        ma.setHours(ma.getHours() + 23);
    }

     if(skip != 0) {
        results.records =  Leads.find({
            lastactivatedate: {$gte: mi,$lt: ma}}, {limit: limit, skip:skip, sort:{ lastactivatedate: -1 }}).fetch();
    }else{
         results.records = Leads.find({lastactivatedate: {$gte: mi,$lt: ma}}, {limit: limit, sort:{ lastactivatedate: -1 }}).fetch();
    }
    results.count = Leads.find({lastactivatedate: {$gte: mi, $lt: ma}}, {limit: limit, sort:{ lastactivatedate: -1 }}).count();
    return results;
}
function filterByupComingDate(min,max,skip, limit){
    var results = {
        count: "",
        records: ""
    }
    var mi = new Date(min);
    var ma = new Date(max);

    if(mi != ma){
        ma.setHours(ma.getHours() + 23);
    }

    if(skip != 0) {
        results.records =  Leads.find({
            nextdate: {$gte: mi,$lt: ma}}, {limit: limit, skip:skip, sort:{ nextdate: -1 }}).fetch();
    }else{
        results.records = Leads.find({nextdate: {$gte: mi,$lt: ma}}, {limit: limit, sort:{ nextdate: -1 }}).fetch();
    }
    results.count = Leads.find({nextdate: {$gte: mi, $lt: ma}}, {sort:{ lastactivatedate: -1 }}).count();
    return results;
}
function defaultFilter(skip, limit){
    var results = {
        count: "",
        records: ""
    }
    if(skip != 0){
        results.records =  Leads.find({}, { limit: limit, skip:skip, sort:{ lastactivatedate: -1 }}).fetch();
        results.count = Leads.find({}).count();
    }else{
        results.records = Leads.find({}, { limit: limit, sort:{ lastactivatedate: -1 }}).fetch();
        results.count = Leads.find({}).count();
    }
    return results;
}
function buildRegExp(searchText) {
    var parts = searchText.trim().split(/[ \-\:]+/);
    return new RegExp("(" + parts.join('|') + ")", "ig");
}