Meteor.methods({
    'uploadFile': function(fileid, filename){
        var fs = Meteor.npmRequire('fs');
        var file = Uploads.find({_id:fileid});
        Meteor.setTimeout(function(){
            console.log('Importing Data..');
            var count = 0;
            var filepath = '../../../cfs/files/uploads/uploads-' + fileid + '-' + filename;
            CSV().from.stream(
                fs.createReadStream(filepath),
                {'escape': '\\'})
                .on('record', Meteor.bindEnvironment(function(row, index){
                    console.log(count);
                    count ++;
                    var protomatch = /^(https?|ftp):\/\//;
                    Leads.insert({
                        'status': row[0],
                        'referaltype': row[1],
                        'actiontype': row[2],
                        'agentid': row[3],
                        'creatorid': row[4],
                        'companytype': row[5],
                        'priority': row[6],
                        'timezone': row[7],
                        'salesagent': row[8],
                        'companyname': row[9],
                        'companyphone': row[10].replace('+1 ',''),
                        'companyemail': row[11],
                        'companyskype': row[12],
                        'companyaddress': row[13].replace(/[,]/g, ""),
                        'companyaddress1': row[14].replace(/[,]/g, ""),
                        'companycity': row[15],
                        'companystate': row[16],
                        'comapnyzip': row[17],
                        'companywebsite': row[18].replace(protomatch, ""),
                        'lastactivatedate': new Date(row[19]),
                        'nextdate': row[20].replace('+1 ',''),
                        'comments': row[21],
                        'contactname': row[22],
                        'contactphone': row[23].replace('+1 ',''),
                        'contactmobilephone': row[24].replace('+1 ',''),
                        'initialwriteup': row[25],
                        'actionlog': row[26],
                        'ipaddress': row[27],
                        'needaction': row[28],
                    });
            }, function(error){
                console.log(error);
            })).on('error', function(err){
            }).on('end', function(){
                console.log('Total Inserted Data: ' + count);
            });
            return count;
        },1000);
    }
});