Meteor.publish('deleteAll', function(){return Leads.remove({});});
Meteor.publish('getLead', function(id){
    var oid = new Meteor.Collection.ObjectID(id);
    var doc = Leads.findOne(oid);
    return doc;
});
Meteor.publish('deleteLead', function(id){
    return Leads.remove(id);
});
Meteor.publish('updateLead', function(obj){
    var myid = new Meteor.Collection.ObjectID(obj.leadId);
    Leads.update({_id: myid},{
        $set: {
            "status" : obj.status,
            "referaltype" : obj.referaltype,
            "actiontype" : obj.actiontype,
            "companytype" : obj.companytype,
            "priority" : obj.priority,
            "timezone" : obj.timezone,
            "companyname" : obj.companyname,
            "companyphone" : obj.companyphone,
            "companyemail" : obj.companyemail,
            "companyskype" : obj.companyskype,
            "companyaddress" : obj.companyaddress,
            "companyaddress1" : obj.companyaddress1,
            "companycity" : obj.companycity,
            "companystate" : obj.companystate,
            "comapnyzip" : obj.companyzip,
            "companywebsite" : obj.companywebsite,
            "lastactivatedate" : date(),
            "comments" : obj.comments,
            "contactname" : obj.contactname,
            "contactphone" : obj.contactphone,
            "contactmobilephone" : obj.contactmobilephone,
            "initialwriteup" : obj.initialwriteup,
            'nextdate': obj.nextdate,
            'salesagent': obj.salesagent,
        }
    });
    //console.log(obj.companyphone);
});
Meteor.publish('countRecords', function(data){
    return Leads.find({}).count();
});
Meteor.publish('filteredLeads', function(options){
    return Leads.find().count();
});
Meteor.publish('countreturnedRecords', function(filterName){
    if(filterName == 'textbox'){
        return  filterBy(searchText, skip);
    }

    if(filterName == 'dropdown'){
        return filterBymultiple(filters, skip);
    }

    if(filterName == 'date'){
        var min = options.min;
        var max = options.max;
        return filterByDate(min,max,skip);
    }

    if(filterName == 'default'){
        return defaultFilter(skip);
    }
});





